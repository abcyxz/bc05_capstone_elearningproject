import axios from "axios";

const api = axios.create({
  baseURL: "https://elearningnew.cybersoft.edu.vn/api/",
});

api.interceptors.request.use((config) => {
  config.headers = {
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNSIsIkhldEhhblN0cmluZyI6IjAxLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTU3NzYwMDAwMCIsIm5iZiI6MTY2MjMxMDgwMCwiZXhwIjoxNjg1NzI1MjAwfQ.Dy6ptxrq8hSICX31ALneG5jR5A2yC8FKcoHtGfXpSi0",
    Authorization: localStorage.getItem("UserClient")
      ? `Bearer ${JSON.parse(localStorage.getItem("UserClient")).accessToken}`
      : localStorage.getItem("AdminClient")
      ? `Bearer ${JSON.parse(localStorage.getItem("AdminClient")).accessToken}`
      : "",
  };
  return config;
});

export default api;
